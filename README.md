## Debug

Use nginx (Apache will not load the js file.) to start the code. 

Go to localhost:<nginxport>/bdbookmarklet.js to see if it start right.

Modify index.php, js should direct to your localhost:<nginxport>/bdbookmarklet.js. For example: js=http%3A%2F%2Flocalhost%3A7888%2Fbdbookmarklet.js

Go to localhost:<nginxport> and it will direct to bookmarklet.php.

Drag the BrownDog picture into your browser bookmarklet

On anyother page, click browndog bookmarklet and type in token

For developing bdbookmarklet.js, you only need to reload localhost:<nginxport>/bdbookmarklet.js and no need to repeat the whole process. 