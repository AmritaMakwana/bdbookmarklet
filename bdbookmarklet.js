console.log("Brown Dog Bookmarklet started");

var bd = "https://bd-api.ncsa.illinois.edu"
var token = getCookie('token');
var baseurl = "https://browndog.ncsa.illinois.edu";
var cachebuster = ('?v=' + (new Date()).getTime());
var modalTemplate;
var html;
var uploadsPromises = [];
var filesInfo = new Array();
var outstandingExtractions = new Array();
var tags = {};
var failedCountDown;
var sucessfullyDone;
var countDone;
var status = '';
var datadocs = {};
var index;
var statusCheck;
var uploadStatus=false;
var uploadCount;
var failCount=0;
var undefinedStatu=0;
var h=0;
var b=0;

var stylesheets = [
'https://jqwidgets.com/public/jqwidgets/styles/jqx.base.css',
baseurl + '/bdbookmarklet/css/dts/dtsstylediv.css',
baseurl + '/bdbookmarklet/css/dts/bootstrap.bd.css',
baseurl + '/bdbookmarklet/css/dts/bootstrap-theme.bd.css',
baseurl + '/bdbookmarklet/css/dap/menu.css'
];

var scripts = [
'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js',
'https://jqwidgets.com/public/jqwidgets/jqx-all.js',
baseurl + '/bdbookmarklet/js/lunr.js',
baseurl + '/bdbookmarklet/js/bootstrap.min.js',
baseurl + '/bdbookmarklet/js/handlebars-v1.3.0.js',
baseurl + '/bdbookmarklet/js/handlebars-loader.js'
];

//var jqpath = "//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.js";
var jqpath = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js";

console.log(bd);
console.log(token);

function init(callback) {
  //Create jQuery script element
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = jqpath;
  document.body.appendChild(script);

  //Exit on jQuery load
  script.onload = function(){ 
    //jQuery.noConflict();
    callback(); 
  };
}

//Kick it off
init(function(){
  loadCSS(stylesheets);
  loadJS(scripts);
});

function loadCSS(stylesheets) {
  //Synchronous loop for css files
  jQuery.each(stylesheets, function(i, stylesheet){
    jQuery('<link>').attr({
      href: (stylesheet + cachebuster),
      rel: 'stylesheet'
    }).prependTo('body');
  });
}

function loadJS(scripts){
  //Load the first js file in the array.
  jQuery.getScript(scripts[0] + cachebuster, function(){
		if(scripts.length > 1) {
      //Asyncronous recursion, courtesy Paul Irish.
      loadJS(scripts.slice(1));
    }else{
      checkAuthorization();
    }
  });
}

//Check if authorization is needed for DAP before attempting to add menus
function checkAuthorization() {
  $.ajax({
    headers: {Authorization: token},
    url: bd + '/dap/alive',     //TODO: Should us a bd-api /ok endpoint that requires authentication.
    success: function() {
      document.onkeydown = function(e) {
        var evtobj = window.event ? event : e

        if (evtobj.keyCode == 70 && evtobj.ctrlKey) {
		      console.log('Openning search box.');
		      openSearchBox();
	      }else if(evtobj.keyCode == 72 && evtobj.ctrlKey) {
          splashScreen();
        }
			};

			if (getCookie('splashshown') == '') {
			  splashScreen();
      }

      addMenuToLinks();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Status: ' + jqXHR.status + ', ' + errorThrown);
      token = prompt("Brown Dog API Token");
      console.log(token)
      document.cookie = "token=" + token;
      if(token!=null){
      	checkAuthorization();
  	  }
    }
  });
}

function splashScreen() {
  console.log('splash screen');

  var div1 = $('<div/>')
    .attr('id', 'splash')
    .attr('style', 'background: transparent; position: fixed')
    .appendTo("body");
  
  var div1a = $('<div/>')
    .attr('style', 'background: white')
    .appendTo(div1);

  var div1a_img = $('<img>')
    .attr('src', baseurl + '/bdbookmarklet/images/bdbookmarklet.png')
    .attr('width', '800')
    .appendTo(div1a);
  
  var div1b = $('<div/>')
    .attr('style', 'background: transparent')
    .appendTo(div1);

  $("#splash").jqxWindow({
    position: {x: document.documentElement.clientWidth/2 - 400, y: document.documentElement.clientHeight/2 - 400 - 25},
    width: '1000',
    height: '1000',
    resizable: false,
    showCloseButton: false,
    showAnimationDuration: 1000,
    closeAnimationDuration: 1000,
    isModal: false,
    autoOpen: false
  });

  setTimeout(function(){ $('#splash').jqxWindow('open'); }, 0);
  setTimeout(function(){ $('#splash').jqxWindow('close'); }, 5000);

  document.cookie = "splashshown=1";
  console.log("document.cookie: ", document.cookie);
}

function openSearchBox()
{
  // index definition
  index = lunr(function() {
    this.field('tags', {
      boost : 10
    });
    this.field('metadata', {boost : 10});
    this.field('url');
    this.ref('id');
  });

  // compile modal template and add it to page
  modalTemplate = Handlebars.getTemplate(baseurl + '/bdbookmarklet/handlebars/modal');
  html = modalTemplate({base_url : baseurl + "/bdbookmarklet"});
  jQuery(document.body).append(html);
  
  jQuery("#DTSquery").keyup(function(event){
    if(event.keyCode == 13){
      jQuery("#DTSSearch").click();
    }
  });
  
	jQuery('#DTSSearch').click(searchIndex);
  jQuery('#DTSShow').click(showData);
  jQuery('#DTSIndex').click(submitData);
  
	jQuery('#DTSModal').modal({backdrop: "static"});	//Open modal
  jQuery("#DTSModal").draggable({
    handle: ".modal-header"
  });

	statusCheck = setTimeout(checkResults, 2000);

	console.log("Added search window.");
}

//Check the status of an extraction
function checkfetch(fileid, g) {
  jQuery.ajax({
		headers: {Authorization: token},
    url : bd + "/dts/api/extractions/" + fileid + "/metadata",
    accepts : "application/json",
    success : function(data){
      indexResults(data, g)
    }
  });
}

function indexResults(extraction, g) {
  for ( var l = 0; l < filesInfo.length; l++) {
    if ((filesInfo[l].id) == (extraction.file_id)) {
      filesInfo[l].tags = extraction.tags;

      // execution is done
      if (filesInfo[l].status != 'Done') {
        filesInfo[l].status = extraction.Status;

        // parse tags even if not done
        var tlen = filesInfo[l].tags.length;
        var tagsArr = new Array();

        for ( var m = 0; m < tlen; m++) {
          var vlen = filesInfo[l].tags[m].values.length;

          for ( var n = 0; n < vlen; n++) {
            var t = filesInfo[l].tags[m].values[n];
            tagsArr.push(t);
          }
        }
 
        // check if metadata is present
        getMetadataAboutFile(filesInfo[l], tagsArr);

        // update cache image metadata
        datadocs[filesInfo[l].id] = createdoc(filesInfo[l].id, tagsArr, filesInfo[l].url, filesInfo[l].src);

        outstandingExtractions[g] = setTimeout(checkfetch, 2000, extraction.file_id, g);
      } else {
        //console.log("if status is Done : Do Nothing");
      }

      break;
    }
  }
}

function getMetadataAboutFile(fileInfo, tagsArr) {
  jQuery.ajax({
    headers: {Authorization: token},
    url : bd + "/dts/api/files/" + fileInfo.id + "/metadata.jsonld",
    accepts : "application/json",
    success : function(data){
      console.log(fileInfo.id + ", tags: " + tagsArr.join(" ") + ", metadata: " + JSON.stringify(data));
      filesInfo.metadata = data;

      // index with lunr
      index.add({
        id : fileInfo.id,
        tags : tagsArr.join(" "),
        metadata : JSON.stringify(data)
      });
    }
  });
}

//Create document for lunr
function createdoc(fid, tags, url, src) {
  var x = {
    id : fid,
    tags : tags,
    url : url,
    src : src
  };

  return x;
}

//Check status of uploaded files by checking promises array and set timeout for successful uploads.
function getMetadata(){
  var plen=uploadsPromises.length;

  for(var g=0;g<plen;g++) {
    // is upload successful?
    if(uploadsPromises[g].status==200){
      pid=uploadsPromises[g].responseText;
      var pidstr=pid.substring(7,pid.length-2);
      outstandingExtractions[g] = setTimeout(checkfetch, 5000, pidstr, g);
    } else {
      outstandingExtractions[g] = setTimeout(doNothing,10000,pidstr);
    }
  }
}

function doNothing(id) {
  //console.log("----Doing  Nothing ---");
}

//Check upload promises to see if upload has complite successfully.
function checkUploadStatus(id) {
  for(var u=0;u<uploadsPromises.length;u++){
    if(uploadsPromises[u].status==200){
      if(uploadsPromises[u].responseText.indexOf(id)!=-1){
        return 200;
      }
    }
  }

  return 0;
}

//Callback after upload is successful.
function reqProcess(url, src) {
  return function(fileidjson) {
    var str = fileidjson;
    var furl = url;
    var fileidObj = new Object();
    
    fileidObj.url = furl;
    fileidObj.src = src;
    fileidObj.id = str.id;
    fileidObj.tags = '';
    fileidObj.status = '';
    fileidObj.metadata = [];
    filesInfo.push(fileidObj);
  }
}

//Submit upload
function startUpload(url, src) {
  var fd = {};
  fd['fileurl'] = url;

  var request = jQuery.ajax({
    type : "POST",
    headers: {Authorization: token},
    url : bd + "/dts/api/extractions/upload_url",
    accepts : "application/json",
    processData : false,
    contentType : "application/json",
    data : JSON.stringify(fd),
    success : reqProcess(url, src)
  });

  uploadsPromises.push(request);
}

//Search index
function searchIndex() {
  var keyword = jQuery('#DTSquery').val();
  console.log("Searching for: " + keyword);

  if (keyword != null) {
    // clear table
    jQuery('#DTSSearchResults tbody tr').remove();

    // create row template
    var searchResults = index.search(keyword);
    var slen = searchResults.length;

    if (slen > 0) {
      var r = new Array();

      for ( var i = 0; i < slen; i++) {
        var id = searchResults[i].ref;
        console.log("Search result: ", datadocs[searchResults[i].ref].tags, "   url:", datadocs[searchResults[i].ref].url);
        r.push(datadocs[searchResults[i].ref].url);

				if(datadocs[searchResults[i].ref].src){
          // find image on page
          var original = jQuery("img[src='" + datadocs[searchResults[i].ref].src + "']")[0];
          var rowTemplate = Handlebars.getTemplate(baseurl + '/bdbookmarklet/handlebars/image');

          // make copy to get rendered size
          var image = new Image();
          image.src = original.src;

          // create html
          var html = rowTemplate({ src : datadocs[searchResults[i].ref].url, width: image.width, height: image.height, 
                                   description: original.alt, 
                                   linkback: bd + "/dts/api/files/" + datadocs[searchResults[i].ref].id + "?token=" + token});
				}else{
          // find link on page
          var original = jQuery("a[href='" + datadocs[searchResults[i].ref].url + "']")[0];
   	      var rowTemplate = Handlebars.getTemplate(baseurl + '/bdbookmarklet/handlebars/link');

          //Create html
          var html = rowTemplate({href: datadocs[searchResults[i].ref].url, description: original.text});
				}

        // add to table
        jQuery('#DTSSearchResults tbody').append(html);
      }

      console.log('Found: ' + slen.toString() + ' (' + r + ')');
    } else {
      console.log("Not Found");
    }
  }

  return false;
}

//Check for status of uploads
function trackCounts(){
  b++;
  uploadCount=0;
  failCount=0;
  var plen=uploadsPromises.length; 

  for(var g=0;g<plen;g++){
    if(uploadsPromises[g].status==undefined){
      //console.log("Status undefined for g=",g);
    }else if(uploadsPromises[g].status==200){
      uploadCount++;
    }else{
      failCount++;
    }
  }

  if(plen==(uploadCount+failCount)){
    uploadStatus=true;
    getMetadata();
  }else{
    statusTrackCounts=setTimeout(trackCounts,1000);
  }
}

//Reset all variables to start over (Work in progress, not fully functional).
function reset() {
  clearInterval(statusCheck);

  for ( var k = 0; k < outstandingExtractions.length; k++) {
    clearInterval(outstandingExtractions[k]);
  }

  // ui counters
  jQuery('#DTSDataFound').text(0);
  jQuery('#DTSSuccessfulExtractions').text(0);
  jQuery('#DTSFailedExtractions').text(0);

  data_urls = [];
  uploadsPromises = [];
  filesInfo = new Array();
  outstandingExtractions = new Array();
  tags = {};
  failedCountDown = 0;
  sucessfullyDone = 0;
  countDone = 0;
  status = '';
  datadocs = {};
  console.log("Reset");
}

function showData() {
  //Clear table
  jQuery('#DTSSearchResults tbody tr').remove();

  //Find images
  var images = jQuery('img');

  //Show images
  for (i=0; i<images.length; i++) {
    if (!jQuery(images[i]).prop('src').startsWith("http://browndog.ncsa.illinois.edu/graphics/") && !jQuery(images[i]).prop('src').endsWith("bdbookmarklet.png")) {
      var rowTemplate = Handlebars.getTemplate(baseurl + '/bdbookmarklet/handlebars/image');

      //Make copy to get rendered size
      var image = new Image();
      image.src = images[i].src;

      //Create html
      var html = rowTemplate({src: jQuery(images[i]).prop('src'), width: image.width, height: image.height, description: images[i].alt});

      //Add to page
      jQuery('#DTSSearchResults tbody').append(html);
    }
  }
  
	//Find links
  var links = jQuery('a');

  //Show links
  for (i=0; i<links.length; i++) {
    if (jQuery(links[i]).prop('href') != "http://browndog.ncsa.illinois.edu/" && links[i].id != "DTSIndex" && links[i].id != "DAPLink") {
   	  var rowTemplate = Handlebars.getTemplate(baseurl + '/bdbookmarklet/handlebars/link');

      //Create html
      var html = rowTemplate({href: jQuery(links[i]).prop('href'), description: links[i].text});

      //Add to page
      jQuery('#DTSSearchResults tbody').append(html);
    }
  }
}

function submitData() {
  // clear table
  jQuery('#DTSSearchResults tbody tr').remove();

  //reset();

  // find all images
  data_urls = jQuery('img').map(function(){
    return {url: jQuery(this).prop('src'), src: jQuery(this).attr('src')};
  });

	// find all links 
	var links = jQuery('a');

  for (i=0; i<links.length; i++) {
    if (jQuery(links[i]).prop('href') != "http://browndog.ncsa.illinois.edu/" && links[i].id != "DTSIndex" && links[i].id != "DAPLink") {
			data_urls.push({url: jQuery(links[i]).prop('href'), src: ''});
		}
	}
  
	var num = data_urls.length;

  // upload data
  for (i=0; i<data_urls.length; i++) {
    if (!data_urls[i].url.startsWith("http://browndog.ncsa.illinois.edu/graphics/") && !data_urls[i].url.endsWith("bdbookmarklet.png")) {
      startUpload(data_urls[i].url, data_urls[i].src);
    } else {
      num -= 1;
    }
  }

  // update ui (FIXME skipping browndog icon in the count; handle)
  jQuery('#DTSDataFound').text(num);
  jQuery('#DTSIndex').toggleClass('active');

  // launch dog
  //addGraphic();

  // track submissions
  setTimeout(trackCounts, 1000);
}

// tracking outstanding extractions by successful and failed
function checkResults() {
  countDone=0;
  failedCountDown=0;
  sucessfullyDone=0;

  for ( var j = 0; j < filesInfo.length; j++) {
    if (filesInfo[j].status == 'Processing') {
      //console.log('[check Results]: Processing');
    } else if (filesInfo[j].status == 'Done') {
      countDone++;
      sucessfullyDone++;
      //jQuery('#DTSSuccessfulExtractions').text(sucessfullyDone);
    } else if (filesInfo[j].status == 'Required Extractor is either busy or is not currently running. Try after some time.') {
      //console.log('[check Results]: Required Extractor is either busy or is not currently running. Try after some time.');
    }else{
      if(filesInfo[j].status==''){
        //console.log('[checkResults]: is empty');

        if(checkUploadStatus(filesInfo[j].id)==200){
          //console.log("[checkResults]: wait for execution");
        }else{
          //console.log("[checkResults]: upload has failed, so skip the execution");
          countDone++;
          failedCountDown++;
        }
      }
    }
  }

  // TODO update ui counts
  jQuery('#DTSSuccessfulExtractions').text(sucessfullyDone);
  jQuery('#DTSFailedExtractions').text(failedCountDown);

  if (countDone > 0 && countDone == filesInfo.length) {
    status = 'Done';
    // TODO update ui done
    jQuery('#DTSIndex').toggleClass('active');
    clearTimeout(statusCheck);
  } else {
    statusCheck=setTimeout(checkResults,2000);
  }
}

//Process webpage once loaded
function addMenuToLinks() {
	//Add graphic
	addGraphic();
	
	//Add link back to self	
	var self = $('<a/>')
		.text('Link to this page')
		.attr('style', 'font-size:80%; font-style:italic; font-weight:bold')
		.attr('href', window.location.href)
		.attr('id', 'DAPLink');
		
	$("body").append('<br>');
	$("body").append('<br>');
	$("body").append(self);

	//Proccess links
	$('a').each(function() {
		var link = this;
		var href = this.href;
		var input = href.split('.').pop();

		//Check for parameters
		if(input.indexOf('?') > -1){
			input = input.split('?')[0];
		}

		//Get supported outputs for this file
		if(input){
			console.log('Input: ' + input);

			$.ajax({headers: {Accept: "text/plain", Authorization: token}, url: bd + '/dap/inputs/' + input}).done(function(outputs) {
				if(outputs) {
					outputs = outputs.split("\n");
					outputs = outputs.filter(function(v){return v!==''});
					//console.log(outputs);

					//Replace link with a menu
					var new_link = $('<ul/>').addClass('menu');
					var new_link_item = $('<li/>')
						.bind('mouseover', openMenu)
						.bind('mouseout', closeMenu)
						.appendTo(new_link);
					$(link).clone().appendTo(new_link_item);

					var menu = $('<li/>').appendTo(new_link_item);
					var menu_list = $('<ul/>').appendTo(menu);

					$.each(outputs, function(i) {
						var li = $('<li/>')
							.on('DOMMouseScroll', scrollMenu)
							.appendTo(menu_list);
							
						var a = $('<a/>')
              .attr('id', 'DAPLink')
							.attr('href', '#')
							.data('href', href)
							.data('output', outputs[i])
							.on('click', convert)
							.text(outputs[i])
							.appendTo(li);
					});

					$(link).replaceWith(new_link);
				}
			});
		}
	});
};

//Issue a conversion request
function convert() {
	console.log($(this).data('href') + ' -> ' + $(this).data('output'));

	//Set cursor to wait
	$('html,body').css('cursor', 'wait');
	$('a').each(function() {
		$(this).css('cursor', 'wait');
	});

	$.ajax({
    headers: {Accept: "text/plain", Authorization: token},
		url: bd + '/dap/convert/' + $(this).data('output') + '/' + encodeURIComponent($(this).data('href')),
	}).then(function(result) {
		redirect(result);
	});
}

//Redirect to the given url once it exists
function redirect(url) {
	//console.log('Redirecting: ' + url);

	$.ajax({
		//xhrFields: { withCredentials: true },
		//crossDomain: true,
		//type: 'HEAD',
    headers: {Authorization: token},
   	url: url,
		success: function() {
			console.log('Redirected: ' + url);
			//window.location.href = url;
			window.location.href = addAuthentication(url);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Status: ' + jqXHR.status + ', ' + errorThrown);

			if(jqXHR.status == 0) {
				console.log('Redirected: ' + url);
				//window.location.href = url;
				window.location.href = addAuthentication(url);
			}else{
				setTimeout(function() {redirect(url);}, 1000);
			}
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');

	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while(c.charAt(0)==' ') c = c.substring(1);
		if(c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}

	return '';
} 

function addAuthentication(url) {
	if(token) {
		url = url + "?token=" + token;
	}

	return url;
}

function openMenu() {
	$(this).find('ul').css('visibility', 'visible');	
}
		
function closeMenu() {
	$(this).find('ul').css('visibility', 'hidden');	
}

function scrollMenu(event) {
	if(event.originalEvent.detail > 0) {
		$(this).siblings().first().appendTo(this.parentNode);
	} else {
		$(this).siblings().last().prependTo(this.parentNode);
	}
	
	event.originalEvent.preventDefault();
}

//Brown Dog graphic
function addGraphic() {
	//Preload images
	//$.get('http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif');
	//$.get('http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif');

	var graphic = $('<img>')
		.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif')
		.attr('width', '25')
		.attr('id', 'graphic')
		.css('position', 'fixed')
		.css('left', '0px')
		.css('bottom', '25px');
	$("body").append(graphic);

	setTimeout(moveGraphicRight, 10);
}

function moveGraphicRight() {
	var graphic = document.getElementById('graphic');
	graphic.style.left = parseInt(graphic.style.left) + 25 + 'px';

	if(parseInt(graphic.style.left) < $(window).width() - 50) {
		setTimeout(moveGraphicRight, 10);
	} else {
		//graphic.remove();
		graphic.parentNode.removeChild(graphic);

		//Add powered by graphic
		graphic = $('<img>')
			//.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif')
			.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/PoweredBy.gif')
			.attr('width', '100');

		var link = $('<a/>')
			.attr('href', 'http://browndog.ncsa.illinois.edu')
			.attr('id', 'poweredby')
			.css('position', 'fixed')
			.css('right', '10px')
			.css('bottom', '10px')
			.append(graphic);

		$("body").append(link);
	}
}
