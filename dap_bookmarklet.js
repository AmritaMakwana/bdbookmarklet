var bd = "https://bd-api.ncsa.illinois.edu";
var token = getCookie('token');

console.log(bd);
console.log(token);

//Load CSS
var css = document.createElement('link');
css.rel = 'stylesheet';
css.type = 'text/css';
css.href = 'http://browndog.ncsa.illinois.edu/bdbookmarklet/css/dap/menu.css';
document.getElementsByTagName('head')[0].appendChild(css);

//Load JQuery
//if(!window.jQuery) {
	var jq = document.createElement('script');
	jq.type = 'text/javascript';
	jq.addEventListener('load', checkAuthorization);
	jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
	document.getElementsByTagName('head')[0].appendChild(jq);
//} else {
//	checkAuthorization();
//}

//Check if authorization is needed for DAP before attempting to add menus
function checkAuthorization() {
	$.ajax({
    headers: {Authorization: token},
    url: bd + '/dap/alive',     //TODO: Should us a bd-api /ok endpoint that requires authentication.
		success: function() {
			console.log("success message");
			addMenuToLinks();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Status: ' + jqXHR.status + ', ' + errorThrown);
			
			token = prompt("Token");
			document.cookie = "token=" + token;

			checkAuthorization();
		}
	});
}

//Process webpage once loaded
function addMenuToLinks() {
	//Add graphic
	addGraphic();
	
	//Add link back to self	
	var self = $('<a/>')
		.text('This file')
		.attr('href', window.location.href)
		.attr('id', 'self');
		
	$("body").append('<br>');
	$("body").append(self);

	//Proccess links
	$('a').each(function() {
		var link = this;
		var href = this.href;
		var input = href.split('.').pop();

		//Check for parameters
		if(input.indexOf('?') > -1){
			input = input.split('?')[0];
		}

		//Get supported outputs for this file
		if(input){
			console.log('Input: ' + input);

			$.ajax({headers: {Accept: "text/plain", Authorization: token}, url: bd + '/dap/inputs/' + input}).done(function(outputs) {
				if(outputs) {
					outputs = outputs.split("\n");
					outputs = outputs.filter(function(v){return v!==''});
					//console.log(outputs);

					//Replace link with a menu
					var new_link = $('<ul/>').addClass('menu');
					var new_link_item = $('<li/>')
						.bind('mouseover', openMenu)
						.bind('mouseout', closeMenu)
						.appendTo(new_link);
					$(link).clone().appendTo(new_link_item);

					var menu = $('<li/>').appendTo(new_link_item);
					var menu_list = $('<ul/>').appendTo(menu);

					$.each(outputs, function(i) {
						var li = $('<li/>')
							.on('DOMMouseScroll', scrollMenu)
							.appendTo(menu_list);
							
						var a = $('<a/>')
							.attr('href', '#')
							.data('href', href)
							.data('output', outputs[i])
							.on('click', convert)
							.text(outputs[i])
							.appendTo(li);
					});

					$(link).replaceWith(new_link);
				}
			});
		}
	});
};

//Issue a conversion request
function convert() {
	console.log($(this).data('href') + ' -> ' + $(this).data('output'));

	//Set cursor to wait
	$('html,body').css('cursor', 'wait');
	$('a').each(function() {
		$(this).css('cursor', 'wait');
	});

	$.ajax({
    headers: {Accept: "text/plain", Authorization: token},
		url: bd + '/dap/convert/' + $(this).data('output') + '/' + encodeURIComponent($(this).data('href')),
	}).then(function(result) {
		redirect(result);
	});
}

//Redirect to the given url once it exists
function redirect(url) {
	//console.log('Redirecting: ' + url);

	$.ajax({
		//xhrFields: { withCredentials: true },
		//crossDomain: true,
		//type: 'HEAD',
    headers: {Authorization: token},
   	url: url,
		success: function() {
			console.log('Redirected: ' + url);
			//window.location.href = url;
			window.location.href = addAuthentication(url);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Status: ' + jqXHR.status + ', ' + errorThrown);

			if(jqXHR.status == 0) {
				console.log('Redirected: ' + url);
				//window.location.href = url;
				window.location.href = addAuthentication(url);
			}else{
				setTimeout(function() {redirect(url);}, 1000);
			}
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');

	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while(c.charAt(0)==' ') c = c.substring(1);
		if(c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}

	return '';
} 

function addAuthentication(url) {
	if(token) {
		url = url + "?token=" + token;
	}

	return url;
}

function openMenu() {
	$(this).find('ul').css('visibility', 'visible');	
}
		
function closeMenu() {
	$(this).find('ul').css('visibility', 'hidden');	
}

function scrollMenu(event) {
	if(event.originalEvent.detail > 0) {
		$(this).siblings().first().appendTo(this.parentNode);
	} else {
		$(this).siblings().last().prependTo(this.parentNode);
	}
	
	event.originalEvent.preventDefault();
}

//Brown Dog graphic
function addGraphic() {
	//Preload images
	//$.get('http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif');
	//$.get('http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif');

	var graphic = $('<img>')
		.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif')
		.attr('width', '25')
		.attr('id', 'graphic')
		.css('position', 'fixed')
		.css('left', '0px')
		.css('bottom', '25px');
	$("body").append(graphic);

	setTimeout(moveGraphicRight, 10);
}

function moveGraphicRight() {
	var graphic = document.getElementById('graphic');
	graphic.style.left = parseInt(graphic.style.left) + 25 + 'px';

	if(parseInt(graphic.style.left) < $(window).width() - 50) {
		setTimeout(moveGraphicRight, 10);
	} else {
		//graphic.remove();
		graphic.parentNode.removeChild(graphic);

		//Add powered by graphic
		graphic = $('<img>')
			//.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif')
			.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/PoweredBy.gif')
			.attr('width', '100');

		var link = $('<a/>')
			.attr('href', 'http://browndog.ncsa.illinois.edu')
			.attr('id', 'poweredby')
			.css('position', 'fixed')
			.css('right', '10px')
			.css('bottom', '10px')
			.append(graphic);

		$("body").append(link);
	}
}
